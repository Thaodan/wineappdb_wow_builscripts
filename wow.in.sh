#!/bin/bash
APPNAME="World of Warcraft"
APPPATH='@GAMEDIR@'
#user_data=$HOME/.games/Skyrim
commands_s=( g G l h e )
commands_l=( game game-32 launcher help end )
exe=( game game32 launcher display_help kill_game )
_game=Wow-64.exe 
_game_32=Wow.exe
_launcher="World of Warcraft Launcher.exe"
_agent=Agent.exe
PREFIX=/home/bidar/.local/share/wineprefixes/wow64
default_exe=$exe
#_wine_args="explorer /desktop=Wow,3840x2160"
HELP_MSG="options: -g --game start $APPNAME
         -G  --game-32 start $APPNAME in 32bit mode
	 -l  --launcher start $APPNAME launcher
 	 -h  --help print this help
	 -e  --end $APPNAME"

# enable __GL_THREADED_OPTIMIZATION
export __GL_THREADED_OPTIMIZATION=1
export STAGING_WRITECOPY=1
export STAGING_RT_PRIORITY_SERVER=90
export __GL_SYNC_TO_VBLANK=0
#export LD_PRELOAD="libpthread.so.0 libGL.so.1"
export STAGING_SHARED_MEMORY=1
#WDEBUG=+tid,seh,d3d9,d3d,d3d_shader
WDEBUG=-all
WINE=wine64
LIBUSEAPP_LVL=2

#\\ifdef WINEPATH
WINEPATH="@WINEPATH@"
#\\endif
game() {
    WINE=wine64
    rm -rf "Cache"
    wine_args="$_wine_args" exec_exe  $_game -d3d11    #pkill $_agent
}

game32()
{
    WINE=wine
    rm -rf "Cache"
    wine_args="$_wine_args" exec_exe  $_game_32 -d3d9 -noautolaunch64bit
	#echo32
}
launcher() {
   # WINE=wine
    rm -rf "Cache"
#    wine_args="$_wine_args" 
exec_exe "$_launcher"
    pkill $_agent
}


kill_game()
{
    pkill $_game
}
. @libdir@/libuse/libuse
