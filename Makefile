# global file that calls all other modules
include rules.mk
MODULES = wine libsh usenew

all: $(MODULES)

$(MODULES): 
	$(MAKE) -C $(@)

install: 
	for target in $(MODULES) ; do $(MAKE) -C $$target $(@);done

wow: wow.in.sh
	$(SHPP) -Dlibdir=$(libdir) -DWINEPATH=$(PREFIX) -DGAMEDIR=$(GAMEDIR)

.PHONY: clean install $(MODULES)
